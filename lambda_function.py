import json
import sys
import logging
import rds_config
import psycopg2
#rds settings
rds_host  = rds_config.db_host
name = rds_config.db_username
password = rds_config.db_password
db_name = rds_config.db_name

logger = logging.getLogger()
logger.setLevel(logging.INFO)

connectionString = "postgres://%s:%s@%s:5432/%s" % (name, password, rds_host, db_name)
try:
    conn = psycopg2.connect(connectionString)
except ConnectionError as e:
    logger.error("ERROR: Unexpected error: Could not connect to MySQL instance.")
    logger.error(e)
    sys.exit()

def lambda_handler(event, context):
    headers = event.get('headers')
    accept = None
    if headers is not None:
        accept = headers.get('Accept')
    
    parameter = event.get('id')
    item_count = 0
    items = []
    with conn.cursor() as cur:
        if parameter is not None:
            cur.execute("SELECT a.uuid, a.user_uuid, a.name, a.description, a.status, a.is_public, a.deadline, a.date_created, a.date_modified, b.name user_name, b.email user_email, b.department user_department FROM incidents a INNER JOIN users b ON a.user_uuid = b.uuid WHERE a.is_public = True AND a.uuid = %s" % parameter)
        else:
            cur.execute("SELECT a.uuid, a.user_uuid, a.name, a.description, a.status, a.is_public, a.deadline, a.date_created, a.date_modified, b.name user_name, b.email user_email, b.department user_department FROM incidents a INNER JOIN users b ON a.user_uuid = b.uuid WHERE a.is_public = True")
        
        for row in cur:
            items.append(row)
            item_count += 1
            
    if accept == 'application/xml':
        body = listToXml(items)
    else:
        body = listToJson(items)
        
    conn.commit()
    
    return {
        'statusCode': 200,
        'body': body
    }

def listToXml(items):
    list = "<incidents>"
    for item in items:
        list += '<incident id="%s">' % item["uuid"]
        list += '<user id="%s">' % item["user_uuid"]
        list += '<name>' + item["user_name"] + '</name>'
        list += '<email>' + item["user_email"] + '</email>'
        list += '<department>' + item["user_department"] + '</department>'
        list += '</user>'
        list += '<name>' + item["name"] + '</name>'
        list += '<description>' + item["description"] + '</description>'
        list += '<status>' + item["status"] + '</status>'
        list += '<date_created>' + item["date_created"] + '</date_created>'
        list += '</incident>'
    list += "</incidents>"
    return list

def listToJson(items):
    data = {}
    incidents = []
    for item in items:
        incidents.append({
            "id": item["id"],
            "user": {
                "id": item["user_uuid"],
                "name": item["user_name"],
                "email": item["user_email"],
                "department": item["user_department"],
            },
            "name": item["name"],
            "description": item["description"],
            "status": item["status"],
            "date_created": item["date_created"],
        })
    if len(incidents) > 1:
        data = {
            "incidents": json.dumps(incidents)
        }
    elif len(incidents) == 1:
        data = {
            "incident": json.dumps(incidents.get(0))
        }
    return data
    